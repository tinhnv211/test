
import os
import json
import random
import datetime
import sys
import math
import numpy as np
import cv2
import skimage.draw
import time


ROOT_DIR = os.path.abspath("./")
MODEL_DIR = os.path.join(ROOT_DIR, "logs")

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn.config import Config
from mrcnn import model as modellib, utils
# import model as modellib

list_classes = ["Button"]
# list_classes = ["button", "title", "input", "text_area"]
SOURCE_CLASS = 'image_processing_redo'
DATASET_DIR = os.path.join(ROOT_DIR, "dataset")
WEIGHTS_PATH = os.path.join(ROOT_DIR, "code/mask_rcnn.h5")


class ImageProcessingConfig(Config):
    NAME = "image_processing_redo"

    GPU_COUNT = 1
    IMAGES_PER_GPU = 2

    NUM_CLASSES = 1 + len(list_classes)  # background + length of labels

    IMAGE_RESIZE_MODE = "square"
    STEPS_PER_EPOCH = 100
    DETECTION_MIN_CONFIDENCE = 0.9

class ImageDataset(utils.Dataset):
    def load_dataset(self, subset):
        assert subset in ["train", "val"]
        for (i, item) in enumerate(list_classes):
            self.add_class(SOURCE_CLASS, i + 1, item)
        dataset_dir = os.path.join(DATASET_DIR, subset)

        annotations = json.load(open(os.path.join(dataset_dir, "label.json")))
        annotations = annotations['_via_img_metadata']
        annotations = list(annotations.values())  # don't need the dict keys
        annotations = [a for a in annotations if a['regions']]

        for a in annotations:
            # regions = [r['shape_attributes'] for r in a['regions']]
            regions = []
            for item in a['regions']:
                temp = item['shape_attributes']
                temp['class_id'] = item['region_attributes']['Label']
                regions.append(temp)

            image_path = os.path.join(dataset_dir, a['filename'])
            image = skimage.io.imread(image_path)
            height, width = image.shape[:2]
            self.add_image(
                SOURCE_CLASS,
                image_id=a['filename'],  # use file name as a unique image id
                path=image_path,
                width=width, height=height,
                regions=regions)

    def load_mask(self, image_id):
        image_info = self.image_info[image_id]
        mask = np.zeros([image_info["width"], image_info["height"], len(image_info["regions"])],
                        dtype=np.uint8)

        for i, p in enumerate(image_info["regions"]):
            # Get indexes of pixels inside the polygon and set them to 1
            if p['name'] == 'rect':
                # cv2.rectangle(image, (p['x'], p['y']), (p['x'] + p['width'], p['y'] + p['height']), 1, -1)
                to_width = np.minimum(p['x'] + p['width'], image_info["width"])
                to_height = np.minimum(p['y'] + p['height'], image_info['height'])
                start, end = [p['x'], to_width, to_width, p['x']], [p['y'], p['y'], to_height, to_height]
                rr, cc = skimage.draw.polygon(start, end)
                # rr, cc = skimage.draw.rectangle((p['x'], p['y']), (p['x'] + p['width'], p['y'] + p['height']))
            elif p['name'] == 'polyline':
                rr, cc = skimage.draw.polygon(p['all_points_y'], p['all_points_x'])
            elif p['name'] == 'circle':
                rr, cc = skimage.draw.circle(p['cx'], p['cy'], p['r'])
            else:
                rr, cc = skimage.draw.polygon(p['all_points_y'], p['all_points_x'])
            mask[rr, cc, i] = 1

        class_ids = np.array([self.class_names.index(s['class_id']) for s in image_info["regions"]])
        return mask, class_ids.astype(np.int32)

def train(init_with):
    model = modellib.MaskRCNN(mode="training", config=config,
                              model_dir=MODEL_DIR)
    if init_with == "last":
        # Load the last model you trained and continue training
        model.load_weights(model.find_last(), by_name=True)
    else:
        # Load weights trained on MS COCO, but skip layers that
        # are different due to the different number of classes
        # See README for instructions to download the COCO weights
        model.load_weights(WEIGHTS_PATH, by_name=True,
                           exclude=["mrcnn_class_logits", "mrcnn_bbox_fc",
                                    "mrcnn_bbox", "mrcnn_mask"])
    print("Begin training: ")
    model.train(dataset_train, dataset_val,
                learning_rate=config.LEARNING_RATE,
                epochs=1,
                layers='heads')

    model.train(dataset_train, dataset_val,
                learning_rate=config.LEARNING_RATE / 10,
                epochs=2,
                layers="all")

def detect(image_path):
    model = modellib.MaskRCNN(mode="inference", config=config,
                              model_dir=MODEL_DIR)
    model.load_weights(model.find_last(), by_name=True)
    image = skimage.io.imread(image_path)
    r = model.detect([image], verbose=1)[0]
    splash = color_splash(image, r['masks'])
    file_name = "splash_{:%Y%m%dT%H%M%S}.png".format(datetime.datetime.now())
    skimage.io.imsave(file_name, splash)
    return

def color_splash(image, mask):
    """Apply color splash effect.
    image: RGB image [height, width, 3]
    mask: instance segmentation mask [height, width, instance count]

    Returns result image.
    """
    # Make a grayscale copy of the image. The grayscale copy still
    # has 3 RGB channels, though.
    gray = skimage.color.gray2rgb(skimage.color.rgb2gray(image)) * 255
    # Copy color pixels from the original color image where mask is set
    if mask.shape[-1] > 0:
        # We're treating all instances as one, so collapse the mask into one layer
        mask = (np.sum(mask, -1, keepdims=True) >= 1)
        splash = np.where(mask, image, gray).astype(np.uint8)
    else:
        splash = gray.astype(np.uint8)
    return splash

if __name__ == '__main__':
    import argparse

    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description='Train Mask R-CNN to detect image section.')
    parser.add_argument("command",
                        metavar="<command>",
                        help="'train' or 'splash'")
    parser.add_argument('--init_with', required=False,
                        default='new',
                        help='Init with new or last')
    parser.add_argument('--image', required=False,
                        metavar="path or URL to image",
                        help='Image to apply the color splash effect on')
    parser.add_argument('--video', required=False,
                        metavar="path or URL to video",
                        help='Video to apply the color splash effect on')
    args = parser.parse_args()

    config = ImageProcessingConfig()
    config.display()
    # Training dataset
    dataset_train = ImageDataset()
    dataset_train.load_dataset('train')
    dataset_train.prepare()

    # Validation dataset
    dataset_val = ImageDataset()
    dataset_val.load_dataset('val')
    dataset_val.prepare()

    if args.command == "train":
        train(args.init_with)
    elif args.command == "detect":
        detect()
