"""
Mask R-CNN
Configurations and data loading code for the synthetic Shapes dataset.
This is a duplicate of the code in the noteobook train_shapes.ipynb for easy
import into other notebooks, such as inspect_model.ipynb.

Copyright (c) 2017 Matterport, Inc.
Licensed under the MIT License (see LICENSE for details)
Written by Waleed Abdulla
"""

import os
import json
import random
import sys
import math
import numpy as np
import cv2
import skimage.draw
import time

# Root directory of the project
ROOT_DIR = os.path.abspath("./")

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn.config import Config
from mrcnn import model as modellib, utils

SOURCE_CLASS = 'labels'
WEIGHTS_PATH = os.path.join(ROOT_DIR, "code/mask_rcnn.h5")
DATASET_DIR = os.path.join(ROOT_DIR, "dataset")
DEFAULT_LOGS_DIR = os.path.join(ROOT_DIR, "logs")
DEFAULT_RESULT_DIR = os.path.join(ROOT_DIR, "detect")

class ImageProcessingConfig(Config):
    """Configuration for training on the toy shapes dataset.
    Derives from the base Config class
    and overrides values specific
    to the toy shapes dataset.
    """
    # Give the configuration a recognizable name
    NAME = "image_processing"

    # Train on 1 GPU and 8 images per GPU. We can put multiple images on each
    # GPU because the images are small. Batch size is 8 (GPUs * images/GPU).
    GPU_COUNT = 1
    IMAGES_PER_GPU = 2

    # Number of classes (including background)
    NUM_CLASSES = 1 + 4  # background + 4 labels

    # Use small images for faster training. Set the limits of the small side
    # the large side, and that determines the image shape.
    IMAGE_RESIZE_MODE = "crop"
    IMAGE_MIN_DIM = 512
    IMAGE_MAX_DIM = 512
    IMAGE_MIN_SCALE = 2.0

    # Use smaller anchors because our image and objects are small
    RPN_ANCHOR_SCALES = (8, 16, 32, 64, 128)  # anchor side in pixels

    # Reduce training ROIs per image because the images are small and have
    # few objects. Aim to allow ROI sampling to pick 33% positive ROIs.
    TRAIN_ROIS_PER_IMAGE = 32

    # Use a small epoch since the data is simple
    STEPS_PER_EPOCH = 100

    # use small validation steps since the epoch is small
    VALIDATION_STEPS = 5

    POST_NMS_ROIS_TRAINING = 1000
    POST_NMS_ROIS_INFERENCE = 2000

class InferenceConfig(ImageProcessingConfig):
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1
    IMAGE_MIN_DIM = 512
    IMAGE_MAX_DIM = 512

class ImageDataset(utils.Dataset):
    """Generates the shapes synthetic dataset. The dataset consists of simple
    shapes (triangles, squares, circles) placed randomly on a blank surface.
    The images are generated on the fly. No file access required.
    """

    def load_dataset(self, subset):
        """Generate the requested number of synthetic images.
        count: number of images to generate.
        height, width: the size of the generated images.
        """
        # Add classes
        self.add_class(SOURCE_CLASS, 1, "button")
        self.add_class(SOURCE_CLASS, 2, "title")
        self.add_class(SOURCE_CLASS, 3, "input")
        self.add_class(SOURCE_CLASS, 4, "text_area")

        # Train or validation dataset?
        assert subset in ["train", "val"]
        dataset_dir = os.path.join(DATASET_DIR, subset)

        # Load annotations
        # VGG Image Annotator (up to version 1.6) saves each image in the form:
        # { 'filename': '28503151_5b5b7ec140_b.jpg',
        #   'regions': {
        #       '0': {
        #           'region_attributes': {},
        #           'shape_attributes': {
        #               'all_points_x': [...],
        #               'all_points_y': [...],
        #               'name': 'polygon'}},
        #       ... more regions ...
        #   },
        #   'size': 100202
        # }
        # We mostly care about the x and y coordinates of each region
        # Note: In VIA 2.0, regions was changed from a dict to a list.
        annotations = json.load(open(os.path.join(dataset_dir, "label.json")))
        annotations = annotations['_via_img_metadata']
        annotations = list(annotations.values())  # don't need the dict keys

        # The VIA tool saves images in the JSON even if they don't have any
        # annotations. Skip unannotated images.
        annotations = [a for a in annotations if a['regions']]

        # Add images
        for a in annotations:
            # Get the x, y coordinaets of points of the polygons that make up
            # the outline of each object instance. These are stores in the
            # shape_attributes (see json format above)
            # The if condition is needed to support VIA versions 1.x and 2.x.
            polygons = []
            for item in a['regions']:
                temp = item['shape_attributes']
                temp['class_id'] = item['region_attributes']['Label']
                polygons.append(temp)

            # load_mask() needs the image size to convert polygons to masks.
            # Unfortunately, VIA doesn't include it in JSON, so we must read
            # the image. This is only managable since the dataset is tiny.
            image_path = os.path.join(dataset_dir, a['filename'])
            image = skimage.io.imread(image_path)
            height, width = image.shape[:2]

            self.add_image(
                SOURCE_CLASS,
                image_id=a['filename'],  # use file name as a unique image id
                path=image_path,
                width=width, height=height,
                polygons=polygons)

    def image_reference(self, image_id):
        """Return the shapes data of the image."""
        info = self.image_info[image_id]
        return "dataset/train/" + info['id']

    def load_mask(self, image_id):
        """Generate instance masks for an image.
       Returns:
        masks: A bool array of shape [height, width, instance count] with
            one mask per instance.
        class_ids: a 1D array of class IDs of the instance masks.
        """
        # If not a image dataset image, delegate to parent class.
        # image_info = self.image_info[image_id]
        # if image_info["source"] != SOURCE_CLASS:
        #     return super(self.__class__, self).load_mask(image_id)

        # Convert polygons to a bitmap mask of shape
        # [height, width, instance_count]
        info = self.image_info[image_id]
        image = skimage.io.imread("dataset/train/" + info['id'])
        mask = np.zeros([info["height"], info["width"], len(info["polygons"])],
                        dtype=np.uint8)
        for i, p in enumerate(info["polygons"]):
            # Get indexes of pixels inside the polygon and set them to 1
            if p['name'] == 'rect':
                cv2.rectangle(image, (p['x'], p['y']), (p['x'] + p['width'], p['y'] + p['height']), 1, -1)
                # to_width = np.minimum(p['x'] + p['width'], info["width"])
                # to_height = np.minimum(p['y'] + p['height'], info['height'])
                # start, end = [p['x'], to_width, to_width, p['x']], [p['y'], p['y'], to_height, to_height]
                # rr, cc = skimage.draw.polygon(start, end)
                # rr, cc = skimage.draw.rectangle((p['x'], p['y']), (p['x'] + p['width'], p['y'] + p['height']))
            elif p['name'] == 'polyline':
                rr, cc = skimage.draw.polygon(p['all_points_y'], p['all_points_x'])
            elif p['name'] == 'circle':
                rr, cc = skimage.draw.circle(p['cx'], p['cy'], p['r'])
            else:
                rr, cc = skimage.draw.polygon(p['all_points_y'], p['all_points_x'])
            # mask[rr, cc, i] = 1
            mask[:, :, image.shape[2] : image.shape[2] + image.shape[2]] = image

        # Return mask, and array of class IDs of each instance.
        class_ids = np.array([self.class_names.index(s['class_id']) for s in info["polygons"]])
        return mask.astype(np.bool), class_ids.astype(np.int32)

    def load_image(self, image_id):
        """Generate an image from the specs of the given image ID.
        Typically this function loads the image from a file, but
        in this case it generates the image on the fly from the
        specs in image_info.
        """
        info = self.image_info[image_id]
        # bg_color = np.array(info['bg_color']).reshape([1, 1, 3])
        # image = np.ones([info['
        # height'], info['width'], 3], dtype=np.uint8)
        image = skimage.io.imread("dataset/train/" + info['id'])
        if image.ndim != 3:
            image = skimage.color.gray2rgb(image)
        # If has an alpha channel, remove it for consistency
        if image.shape[-1] == 4:
            image = image[..., :3]
        return image

def train(init_with):
    model = modellib.MaskRCNN(mode="training", config=config,
                              model_dir=DEFAULT_LOGS_DIR)

    if init_with == "imagenet":
        model.load_weights(model.get_imagenet_weights(), by_name=True)
    elif init_with == "last":
        # Load the last model you trained and continue training
        model.load_weights(model.find_last(), by_name=True)
    else:
        # Load weights trained on MS COCO, but skip layers that
        # are different due to the different number of classes
        # See README for instructions to download the COCO weights
        model.load_weights(WEIGHTS_PATH, by_name=True,
                           exclude=["mrcnn_class_logits", "mrcnn_bbox_fc",
                                    "mrcnn_bbox", "mrcnn_mask"])
    print("Begin training: ")
    model.train(dataset_train, dataset_val,
                learning_rate=config.LEARNING_RATE,
                epochs=1,
                layers='heads')

    model.train(dataset_train, dataset_val,
                learning_rate=config.LEARNING_RATE / 10,
                epochs=2,
                layers="all")

def detect():
    inference_config = InferenceConfig()
    # Recreate the model in inference mode
    model = modellib.MaskRCNN(mode="inference",
                              config=inference_config,
                              model_dir=DEFAULT_LOGS_DIR)

    model_path = model.find_last()
    print("load weight from " + model_path)
    model.load_weights(model_path, by_name=True)

    # image_id = 0
    # original_image, image_meta, gt_class_id, gt_bbox, gt_mask = modellib.load_image_gt(dataset_val, inference_config,
    #                                                                                    image_id, use_mini_mask=False)
    # results = model.detect([original_image], verbose=1)[0]
    # print(results)
    #
    # sys.exit()

    # images = [item['path'] for item in dataset_val.image_info]
    # image_id = random.randint(0, len(images) - 1)
    # image_name = images[image_id]
    # image = skimage.io.imread(images[image_id])
    image_name = 'Picture01.png'
    image = skimage.io.imread("dataset/train/" + image_name)
    print(image.shape)
    sys.exit()

    results = model.detect([image], verbose=1)[0]

    splash = color_splash(image, results['masks'])
    cv2.imwrite(os.path.join(DEFAULT_RESULT_DIR, time.strftime("%Y%m%d") + image_name), splash)
    print(results)


def color_splash(image, mask):
    """Apply color splash effect.
    image: RGB image [height, width, 3]
    mask: instance segmentation mask [height, width, instance count]

    Returns result image.
    """
    # Make a grayscale copy of the image. The grayscale copy still
    # has 3 RGB channels, though.
    gray = skimage.color.gray2rgb(skimage.color.rgb2gray(image)) * 255
    # Copy color pixels from the original color image where mask is set
    if mask.shape[-1] > 0:
        # We're treating all instances as one, so collapse the mask into one layer
        mask = (np.sum(mask, -1, keepdims=True) >= 1)
        splash = np.where(mask, image, gray).astype(np.uint8)
    else:
        splash = gray.astype(np.uint8)
    return splash


if __name__ == '__main__':
    import argparse

    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description='Train Mask R-CNN to detect image section.')
    parser.add_argument("command",
                        metavar="<command>",
                        help="'train' or 'splash'")
    parser.add_argument('--logs', required=False,
                        default=DEFAULT_LOGS_DIR,
                        metavar="/path/to/logs/",
                        help='Logs and checkpoints directory (default=logs/)')
    parser.add_argument('--init_with', required=False,
                        default='new',
                        help='Init with new or last')
    parser.add_argument('--image', required=False,
                        metavar="path or URL to image",
                        help='Image to apply the color splash effect on')
    parser.add_argument('--video', required=False,
                        metavar="path or URL to video",
                        help='Video to apply the color splash effect on')
    args = parser.parse_args()

    config = ImageProcessingConfig()
    detect_config = InferenceConfig()
    config.display()
    # Training dataset
    dataset_train = ImageDataset()
    dataset_train.load_dataset('train')
    dataset_train.prepare()

    # Validation dataset
    dataset_val = ImageDataset()
    dataset_val.load_dataset('val')
    dataset_val.prepare()

    if args.command == "train":
        train(args.init_with)
    elif args.command == "detect":
        detect()
